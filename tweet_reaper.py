import requests
import os
import sys
import json
import csv
import datetime
from time import sleep

# This is just a draft written in nano quickly. Don't judge if it does not follow PEP8 or whateva :)
# You need to create a project and app here https://developer.twitter.com/en/portal/dashboard to get the token
# Author: Humi7
# For security reasons keeping token in env var $TWBTOKEN
# example usage: python3 tweet_reaper.py 1370676019628216320
#                python3 tweet_reaper.py PICKUP_ON_FAIL


bearer_token = os.environ['TWBTOKEN']


def connect_to_endpoint(url):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    response = requests.request("GET", url, headers=headers)
    # print(response.status_code)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()

def store_results(json_response, first=False):
    json_log = "Twitzz.json"
    csv_log = "Twitzz.csv"
    if first:
        if os.path.exists(json_log):
            os.remove(json_log)
        if os.path.exists(csv_log):
            os.remove(csv_log)        

        fields = ["author_id", "created_at", "tweet_id", "source", "text"]
        with open(csv_log, 'a') as csvfile:  
            csvwriter = csv.writer(csvfile)  
            csvwriter.writerow(fields)
            
    # print(json.dumps(json_response, indent=2))

    for tweet in json_response["data"]:
        with open(json_log, "a") as file_object:
            file_object.write(json.dumps(tweet, indent=2)+"\n")
        with open(csv_log, 'a') as csvfile:  
            csvwriter = csv.writer(csvfile)  

            row = [ tweet["author_id"], tweet["created_at"], tweet["id"], tweet["source"], tweet["text"] ]
            csvwriter.writerow(row)        

def restore_from_failure_file(filename):
    with open(filename) as file:
        restore = json.load(file)
        return restore["username"], restore["target_tweet"], restore["processed"], restore["total"], restore["next"]

def main():
    TimeStart = datetime.datetime.now()
    if len(sys.argv) != 2:
        raise ValueError('Please provide tweet id !!!')

    target_tweet = sys.argv[1]

    if target_tweet != "PICKUP_ON_FAIL":
        url1='https://api.twitter.com/2/tweets?ids={}&tweet.fields=public_metrics&expansions=referenced_tweets.id,referenced_tweets.id.author_id'.format(target_tweet)
        json_response = connect_to_endpoint(url1)
        # print(json.dumps(json_response, indent=4, sort_keys=True))
        username = json_response["includes"]["users"][0]["username"]
        print('Started getting replies to @{} tweet.'.format(username))
        replies = json_response["data"][0]["public_metrics"]["reply_count"]
        print('There are {} replies to be processed!'.format(replies))

        query='to:{} conversation_id:{} is:reply'.format(username,target_tweet)
        url2='https://api.twitter.com/2/tweets/search/recent?query={}&tweet.fields=attachments,author_id,created_at,public_metrics,source'.format(query)
    
        json_response = connect_to_endpoint(url2)
        store_results(json_response, first=True)
        next=json_response["meta"]["next_token"]
    #    print(next)
        processed = 10
        total = int(replies)
    else:
        username, target_tweet, processed, total, next = restore_from_failure_file('failure.json')
        query='to:{} conversation_id:{} is:reply'.format(username,target_tweet)
        url2='https://api.twitter.com/2/tweets/search/recent?query={}&tweet.fields=attachments,author_id,created_at,public_metrics,source'.format(query)
        print('Picking up after failure.')

    requestz = 0
    print("Processed {} of {} - next {}".format(processed,total,next) )
    while processed<total:
        
        # Below if is due to 450 limit of requests per app per 15min https://developer.twitter.com/en/docs/twitter-api/rate-limits
        requestz += 1
        if requestz > 449:
            TimeNow = datetime.datetime.now()
            timeDiff = TimeNow - TimeStart
            minutes = timeDiff.total_seconds() // 60
            print("Done 450 requests in {} minutes.".format(minutes))
            ntw = 15 - minutes
            print("Need to wait {} minutes now.".format(ntw))
            sleep(ntw*60)
            requestz = 0
            TimeStart = datetime.datetime.now()
            print("Resuming from wait.")
        
        json_response = connect_to_endpoint(url2+'&next_token='+next)
        store_results(json_response)
        recnt=json_response["meta"]["result_count"]
        processed += int(recnt)
        try:
            next=json_response["meta"]["next_token"]
        except:
            print('Looks like we are done here')
            break
        
        print("Processed {} of {} - next {}".format(processed,total,next) )

        
if __name__ == "__main__":
    main()
